library(maptools)
library(foreign)
#library(Deducer)
library(maptools)
library(RColorBrewer)
library(classInt)
library(pROC)
library("plyr")
library("colorRamps")
library(leaflet)
library(rgdal) #for reading/writing geo files
library(rgeos) #for simplification
#library(rJava)
library(sp)

setwd("C:/Users/Usuario/Dropbox/Cursos/EstadisticaAplicadaIIIOtono2016/Mapaindice/MapsAgeb")
source("guardarMapas.R")
setwd("C:/Users/Usuario/Dropbox/Cursos/EstadisticaAplicadaIIIOtono2016/Mapaindice/Mapas")
#DFrur=readShapeSpatial("df_loc_rur.shp")
setwd("C:/Users/Usuario/Dropbox/Cursos/EstadisticaAplicadaIIIOtono2016/Mapaindice")

TipoCol=c(rep("factor",6),rep("numeric",194))
DF=read.table("df.csv",colClasses=TipoCol,header = TRUE,sep=",",na.strings=c("*","N/D"))

I=which(DF$loc=="0000")
DF=DF[-I,]

I=which(DF$loc=="9999")
DF=DF[-I,]

I=which(DF$loc=="9998")
DF=DF[-I,]

aux=as.character(DF$longitud)
longSegundos=substr(aux,5,6)
longMinutos=substr(aux,3,4)
longGrados=substr(aux,1,2)

longSegundos=as.numeric(longSegundos)
longMinutos=as.numeric(longMinutos)
longGrados=as.numeric(longGrados)

longGrados=longGrados+longMinutos/60+longSegundos/3600

aux=as.character(DF$latitud)
latSegundos=substr(aux,5,6)
latMinutos=substr(aux,3,4)
latGrados=substr(aux,1,2)

latSegundos=as.numeric(latSegundos)
latMinutos=as.numeric(latMinutos)
latGrados=as.numeric(latGrados)

latGrados=latGrados+latMinutos/60+latSegundos/3600

pal <- colorQuantile("YlGn", NULL, n = 5)

a=pal(DF$pobtot)
a.t=table(a)
a.t.at=attributes(a.t)
colLegend <- rev(a.t.at$dimnames$a)
Labels=c("0-0.2","0.2-0.4","0.4-0.6","0.6-0.8","0.8-1")

state_popup <- paste0("<strong>MUNICIPIO: </strong>",
                      DF$nom_mun,
                      "<br><strong>STATE: </strong>",
                      DF$nom_ent,
                      "<br><strong>Localidad:</strong>",
                      DF$nom_loc,
                      "<br><strong>POB1:</strong>",
                      DF$pobtot)

mymap=leaflet(data = DF) %>%
  addProviderTiles("CartoDB.Positron") %>%
  addMarkers(~longitud, ~latitud),
fillColor = ~pal(pobtot),
fillOpacity = 0.8,
color = "#BDBDC3",
weight = 1,
popup = state_popup)%>%
  addLegend("bottomright", colors=colLegend, labels=Labels,
            title = "POB1",
            opacity = 1
  )

saveas(mymap,"DF_urb.html")
DF$longitud