matrizTransicion=function( capacidad, poisson_retiro, poisson_arribo, correlacion){
  
  # lambda_1=poisson_arribo - correlacion* sqrt(poisson_arribo*poisson_retiro)
  # lambda_2=poisson_retiro - correlacion* sqrt(poisson_arribo*poisson_retiro)
  lambda_1=poisson_arribo 
  lambda_2=poisson_retiro 
  lambda_1=ifelse(lambda_1<0,0,lambda_1)
  lambda_2=ifelse(lambda_2<0,0,lambda_2)
  
  x=seq(from=-capacidad,to=capacidad)
  ##Probabilidad de caer en cada estado
  prob=sapply(x, pskellam,lambda1=lambda_1 , lambda2=lambda_2 ) %>% data.frame() %>% t() %>%
    `colnames<-` (x)
  prob[is.nan(prob)]=0
  mat=matrix(0,nrow = capacidad+1, ncol = capacidad+1) 
  
  
  
  for(i in 0:capacidad){
    # i=4
    indice=which(colnames(prob)==0)
    ##Se agregan los valores negativos
    prob_temp=(prob[(indice-i): (indice+capacidad-i)])
    ##Probabilidad de pasar de un estado a otro
    res=prob_temp-lag(prob_temp,1)
    res[1]=prob_temp[1]
    res[(capacidad+1)]=1-sum(res[1:capacidad])
    mat[(i+1),]=res
  }
  
  return (mat)
}
