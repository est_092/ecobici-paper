library(tidygraph)
resumen_polanco=readRDS("cache/resumen_polanco")
estaciones=read.csv("data/estaciones.csv")
files=list.files("cache/escenarios/")
# files=files[grepl("n-40", files) &grepl("k-30", files) & grepl("T-16200.RData",files)  ]
files=files[grepl("k-30", files)   & grepl("n-40",files) & (!grepl("linearizacion",files)) ]
x="n-40_k-30_alpha0.0033_T-16200.RData"
files%>%
  map(function(x){
    print(x)
    load(paste0("cache/escenarios/",x))
    specs=x  %>%
      data.frame(text=.) %>%
      separate(text, into=c("N","k","alpha","T"), sep="_") %>%
      # separate(text, into=c("N","k","alpha","T","lin","h","p"), sep="_") %>%
      mutate_at(c("N","k","alpha","T"), function(x){
      # mutate_at(c("N","k","alpha","T","lin","h","p"), function(x){
        x=gsub("[A-z]-","",x) 
        x=gsub("[a-z]","",x)
        x=gsub(".RD","",x) 
        x=as.numeric(x)
      })
    seleccionadas=estaciones %>% filter(districtName=="Polanco") %>% .$id%>%
      head(specs$N)
    
    n=length(seleccionadas) 
    x =gsub(".RData",".RDS",x)
    resumen=readRDS(paste0("cache/resumen_escenarios/",x)) %>%
      mutate(mejora=1-(penalizacion_final/penalizacion_inicial),
             text=x ) %>%
      separate(text, into=c("N","k","alpha","T","lin","h","p"), sep="_") %>%
      mutate_at(c("N","k","alpha","T","lin","h","p"), function(x){
        x=gsub("[A-z]-","",x)
        x=gsub("[a-z]","",x)
        x=gsub(".RDS","",x)
      }) %>%
      select(-c(id,name, coordenadas, longitud, latitud))%>% 
      mutate(mejora_total=penalizacion_inicial-penalizacion_final,
             mejora=(penalizacion_final-penalizacion_inicial) / penalizacion_inicial,
             distancia_optimo= penalizacion_final/penalizacion_optima ) 
    estaciones_g=resumen_polanco %>%
      filter(id %in% seleccionadas) %>%
      mutate(coordenadas=paste0(`location.lat`,",",`location.lon`), 
             longitud=location.lon,
             latitud=location.lat,
             estacion=id, id=row_number()) %>%
      left_join(resumen, by=c("estacion"))
    edges=sol$solution %>% as.data.frame() %>% tibble::rownames_to_column("ID") %>% 
      rename(value=2) %>%
      filter(grepl("y",ID)) %>% mutate(ID=gsub("y","",ID) %>% as.numeric()) %>% 
      filter(!is.na(ID)) %>%
      mutate( origin= ceiling( ID/specs$N) ,destiny= ifelse(ID%%specs$N==0,specs$N,ID%%specs$N)) %>%
      # filter(value==1) %>%
      select(from=origin,to=destiny, weight=value) %>%
      # select(origin=estacion.x,destiny=estacion.y,value) %>%
      filter(weight>0) 
    
    
    # edges %>%group_by(from,to) %>% summarise(n=n()) ->l
    
    graph=tbl_graph(nodes=estaciones_g %>% rename(Id=id,label=name, lat=location.lat,lon=location.lon, demanda_diaria=demanda_diaria),
                    edges=edges %>% select(-weight))
    G=graph%>%
      # mutate(betweenness_centrality = centrality_betweenness(normalized = T)) %>%
      # mutate(closeness_centrality = centrality_closeness(normalized = T))%>%
      # mutate(centrality_degree=centrality_degree(mode='out',normalized = T))%>%
      # mutate(centrality_eigen=centrality_eigen(weights=graph$Frequency,directed=T)) %>%
      set_edge_attr("weight", value=edges$weight)
    
    
    plot_vector<- as.data.frame(cbind(V(G)$lon,V(G)$lat))
    plot_vector1<- as.data.frame(cbind(V(G)$lon,V(G)$lat,V(G)$demanda_diaria, V(G)$demanda_diaria, capacity=V(G)$capacity,
                                       inv_optimo=V(G)$inv_optimo, inv_final=V(G)$inv_final, inv_inicial=V(G)$s0,
                                       estado_inicial=V(G)$estado_inicial, estado_final=V(G)$estado_final)) %>%
      mutate(diferencia=inv_final-inv_inicial)
    
    #we are taking the edgelist which is being used to get the origin and destination of the airport data.
    #edgelist[,1]- takes the origin values and the edgelist[,2] takes the destination values.
    edgelist <- get.edgelist(G)
    # edgelist <-E(G)
    edgelist[,1]<-as.numeric(match(edgelist[,1],V(G)$Id))
    edgelist[,2]<-as.numeric(match(edgelist[,2],V(G)$Id))
    
    #the edges now consists of the edge between the origin and the destination values
    edges_plot <- data.frame(plot_vector[edgelist[,1],], plot_vector[edgelist[,2],],  edges$weight,
                             start=! edges$from %in% edges$to, end=! edges$to %in% edges$from)  
    
    #naming the columns obtained to plot
    colnames(edges_plot) <- c("X1","Y1","X2","Y2","weight","start","end")
    # p=qmap(location="Polanco, México",zoom=14 , source="stamen") 
    # p = qmap(location = c(lon = -99.1952899, lat =19.4329165), zoom = 15, source="stamen",color = "bw")
    
    
    z=p +
      # geom_segment(aes(x=X1, y=Y1, xend = X2, yend = Y2), data=edges_plot,arrow = arrow(length = unit(0.5,"cm"))) +
      geom_point(aes(V1, V2), data=plot_vector1)+
      geom_point(aes(x=V1,y=V2,size=plot_vector1$inv_inicial,colour=as.factor(V1)),plot_vector1)+ 
      # geom_text(aes(x=X2, y=Y2, label=scales::number(weight) ),data=edges_plot, colour="white") +
      # scale_colour_gradient() +
      # theme(legend.position = "left")+
      theme(legend.position = "NULL")+
      # labs(title=paste0("Estado Final: ", "n=",specs$N," k=",specs$k," alpha=",specs$alpha," T=",specs$T )) +
      # scale_colour_continuous(low = "green", high = "red", labels=scales::percent) +
      scale_color_manual(values=wes_palette("FantasticFox1",n=40,type = "continuous"))+
      # scale_colour_continuous(name="Porcentaje de Estado Óptimo",
      #                         low = "green", high = "red", labels=scales::percent) +
      # scale_colour_continuous(low = "grey37", high = "grey3") +
      # geom_point(aes(x=X1, y=Y1 +.0021 ),data=edges_plot[edges_plot$start,],
      #            shape = 25, fill="brown", size = 8 ) +
      # geom_point(aes(x=X2, y=Y2 +.0021 ),data=edges_plot[edges_plot$end,],
      #            shape = 25, fill="brown", size = 8 ) +
      # {if(edges_plot[edges_plot$start,] %>% nrow() >0)
      # {geom_text(aes(x=X1, y=Y1+.0021, label="Inicio"),data=edges_plot[edges_plot$start,], colour="black")}} +
      # {if(edges_plot[edges_plot$end,] %>% nrow() >0)
      # {geom_text(aes(x=X2, y=Y2+.0021, label="Fin"),data=edges_plot[edges_plot$end,], colour="black")}} +
      geom_text(aes(x=V1, y=V2, label=scales::number(inv_inicial)),data=plot_vector1,vjust = -1) +
      # geom_text(aes(x=X2, y=Y2+.0021, label="Fin"),data=edges_plot[edges_plot$end,], colour="black") +
      scale_size_continuous(name="Bicicletas en Inventario", labels=scales::number) +
      ggsave(paste0("output/plots_resultados/comparativa/inicial_",gsub(".RDS",".png",x)))
    
    z=p +
      geom_segment(aes(x=X1, y=Y1, xend = X2, yend = Y2),colour="yellow", data=edges_plot,arrow = arrow(length = unit(0.5,"cm"))) +
      geom_point(aes(V1, V2), data=plot_vector1)+
      geom_point(aes(x=V1,y=V2,size=plot_vector1$inv_final,colour=as.factor(V1)),plot_vector1)+ 
      scale_color_manual(values=wes_palette("FantasticFox1",n=40,type = "continuous"))+
      # scale_color_discrete() +
      # geom_point(aes(x=V1,y=V2,size=plot_vector1$inv_final,colour=plot_vector1$estado_final),plot_vector1)+ 
      # geom_text(aes(x=X2, y=Y2, label=scales::number(weight) ),data=edges_plot, colour="white") +
      # scale_colour_gradient() +
      # theme(legend.position = "left")+
      theme(legend.position = "NULL")+
      # labs(title=paste0("Estado Final: ", "n=",specs$N," k=",specs$k," alpha=",specs$alpha," T=",specs$T )) +
      # scale_colour_continuous(low = "green", high = "red", labels=scales::percent) +
      # scale_colour_continuous(name="Porcentaje de Estado Óptimo",
      #                         low = "green", high = "red", labels=scales::percent) +
     # scale_colour_continuous(low = "grey37", high = "grey3") +
      # geom_point(aes(x=X1, y=Y1 +.0021 ),data=edges_plot[edges_plot$start,],
      #            shape = 25, fill="brown", size = 8 ) +
      # geom_point(aes(x=X2, y=Y2 +.0021 ),data=edges_plot[edges_plot$end,],
      #            shape = 25, fill="brown", size = 8 ) +
      # {if(edges_plot[edges_plot$start,] %>% nrow() >0)
      #   {geom_text(aes(x=X1, y=Y1+.0021, label="Inicio"),data=edges_plot[edges_plot$start,], colour="black")}} +
      # {if(edges_plot[edges_plot$end,] %>% nrow() >0)
      # {geom_text(aes(x=X2, y=Y2+.0021, label="Fin"),data=edges_plot[edges_plot$end,], colour="black")}} +
      geom_text(aes(x=V1, y=V2, label=scales::number(inv_final)),data=plot_vector1,vjust = -1) +
      # geom_text(aes(x=X2, y=Y2+.0021, label="Fin"),data=edges_plot[edges_plot$end,], colour="black") +
      scale_size_continuous(name="Bicicletas en Inventario", labels=scales::number) +
      ggsave(paste0("output/plots_resultados/comparativa/",gsub(".RDS",".png",x)))

  })




z2=p +
  # geom_segment(aes(x=X1, y=Y1, xend = X2, yend = Y2), color='yellow',data=edges_plot,arrow = arrow(length = unit(0.5,"cm"))) +
  geom_point(aes(V1, V2), data=plot_vector1)+
  geom_point(aes(x=V1,y=V2,size=plot_vector1$inv_inicial,colour=plot_vector1$estado_inicial),plot_vector1)+ 
  geom_text(aes(x=V1, y=V2, label=scales::number(inv_inicial)),data=plot_vector1,vjust = -1) +
  # geom_text(aes(x=X2, y=Y2, label=scales::number(weight) ),data=edges_plot, colour="white") +
  # scale_colour_gradient(low = "red", high = "green") +
  theme(legend.position = "left")+
  labs(title="Estado Final") +
  scale_colour_continuous(name="Porcentaje de Estado Óptimo",
                          low = "red", high = "green", labels=scales::percent) 
z2
