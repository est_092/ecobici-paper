library(dplyr)
library(googleway)
library(leaflet)


mykey <- "AIzaSyBwejWA4fl1vSpppztZP4KZdEohFukeDxs"


poly_viajes=viajes %>%
  nrow() %>%
  1:. %>%
  map_dfr(function(x){
    foo <- google_directions(origin = viajes[x,]$coordenadas_origin,
                             destination = viajes[x,]$coordenadas_destiny,
                             key = mykey,
                             mode = "driving",
                             simplify = TRUE)
    
    pl <- decode_pl(foo$routes$overview_polyline$points)%>%
      mutate(id=paste0(viajes[x,]$origin,"-",viajes[x,]$destiny))
  })


# 
# m <- leaflet() %>%
#   addProviderTiles("OpenStreetMap.Mapnik") %>%
#   addPolylines(data = temp, lng = ~lon, lat = ~lat, group = ~region) %>%
#   addMarkers(lng = -87.6266382, lat = 41.8674336,
#              popup = "starting")%>%
#   addMarkers(data = mydf, lng = ~to_long, lat = ~to_lat,
#              popup = "Destination")