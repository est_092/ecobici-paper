library(dplyr)
library(chron)
library(lubridate)
library(xts)
library(skellam)
library(doParallel)
library(padr)
library(purrr)

# load("data/agrupado.RData")





source("lib/matrizTransicion.R")
source("lib/calcula_penalizacion.R")
# load("data/base_full.RData")
# base %>% filter(Ciclo_Estacion_Retiro %in% seleccionadas | Ciclo_Estacion_Arribo %in% seleccionadas) ->base
# saveRDS(base,file = "data/base_polanco.RDS")
base=readRDS("data/base_polanco.RDS")


base %>%
  filter(dia_semana_retiro=="Thursday") %>%
  filter(month(mes_retiro)==6 & year(mes_retiro)==2018 ) %>%
  mutate(Hora_Arribo=floor_date(Hora_Arribo, unit="15 mins"),
         Hora_Retiro=floor_date(Hora_Retiro, unit="15 mins"))  ->b_temp
b_temp%>%
  group_by(id=Ciclo_Estacion_Retiro, Hora=format(Hora_Retiro,"%H:%M:%S"),dia=floor_date(Hora_Retiro,unit = "days")) %>%
  summarise(retiros=n()) %>%
  ungroup() %>%
  full_join({
    b_temp%>%
      group_by(id=Ciclo_Estacion_Arribo, Hora=format(Hora_Arribo,"%H:%M:%S"),dia=floor_date(Hora_Retiro,unit = "days")) %>%
      summarise(arribos=n()) %>%
      ungroup()
  }, by=c("id","Hora","dia")) %>% 
  mutate(Hora=paste0("2019-06-14 ",Hora) %>% as.POSIXct()) %>%
  group_by(id,dia) %>%
  pad(interval = "15 mins", by="Hora") %>%
  ungroup() %>%
  mutate_if(is.numeric,function(x){
    ifelse(is.na(x),0,x)
  })%>%
  group_by(id, Hora) %>%
  mutate(correlacion=cor(retiros,arribos), correlacion=ifelse(is.na(correlacion),0,correlacion)) %>%
  summarise_at(c("retiros","arribos","correlacion"), mean) %>%
  ungroup() %>%
  left_join(capacity , by="id") %>%
  rename(estacion=id, periodo=Hora, capacidad=capacity) %>% mutate(hora=hour(periodo))->dat
  
# estaciones=read.csv("data/estaciones.csv")


  # mutate(Hora_Arribo=cut(Hora_Arribo, breaks = "min"),
  #        Hora_Retiro=cut(Hora_Retiro, breaks = "min"))

# dat=agrupado %>%
#   filter(dia=="jueves") %>% select(-name)
# rm(agrupado)
# estaciones=read.csv("data/estaciones.csv") %>%
#   left_join(capacity, by=c("id")) 
# saveRDS(estaciones, file="cache/estaciones.RDS")
estaciones=readRDS("cache/estaciones.RDS")
# estacion=251

# ##153
# load("data/base_penalizacion.RData")
# estaciones_temp=unique(res$estacion)
# estaciones=setdiff(estaciones,estaciones_temp)

penalizacion=function(id_estacion,base_estaciones, h=1, p=1){
  source("lib/calcula_penalizacion.R")
  library(dplyr)
  library(skellam)
  print(id_estacion)
  capacidad=unique(base_estaciones%>% filter(estacion==id_estacion) %>% select(capacity)) %>%as.numeric()
  0:capacidad %>%
  map_dfr(function(x){
    calcula_penalizacion_acumulada(i0=x,dat=dat,id_estacion=id_estacion, hora_max=23, hora_min=0,h=h, p=p) %>%
      data.frame(penalizacion=.) %>% mutate(i0=x,estacion=id_estacion)
  })
  # res_temp=sapply(0:capacidad,calcula_penalizacion_acumulada,dat=dat,id_estacion=id_estacion, hora_max=23, hora_min=0,h=h, p=p) 
  # res_temp %>%
  #   rbind(0:capacidad , .) %>% 
  #   t() %>% data.frame() %>%
  #   setNames(c("i0","penalizacion","estacion"))
  # return(res_temp)
}



cl <- makeCluster(detectCores ())
registerDoParallel(cl)
clusterExport(cl, c("dat","penalizacion"))
## 1) No referencing

parametros=expand.grid(p=c(1,.5), h=c(1,.5)) %>%
  filter(p!=h | (p==h & p==1) )
# %>% filter(!(p==.5 & h== 1) )


parametros %>%
  # head(1) %>%
  pmap(function(p,h){
   seleccionadas %>%
      # head(2) %>%
      map_dfr(function(x){
       penalizacion(id_estacion = x,base_estaciones = estaciones, h=h,p=p)
      }) ->penalizacion_data
    linearizacion=penalizacion_data %>%
      arrange(estacion,i0) %>%
      group_by(estacion) %>%
      mutate(penalizacion1=lead(penalizacion,1),
             b_i=penalizacion1-penalizacion, 
             a_i=penalizacion- b_i*i0,
             penalizacion=penalizacion) %>%
      select(i0,estacion,b_i,a_i,penalizacion) %>%
      filter(i0!=max(i0)) %>%
      ungroup()
    saveRDS(linearizacion,file = paste0("data/linearizacion_h",h,"_p",p,".RDS"))
    saveRDS(penalizacion_data,file = paste0("data/penalizacion_h",h,"_p",p,".RDS"))
    # res <- foreach(id=estaciones$i, .combine=rbind) %dopar% penalizacion(id_estacion = id, h=h,p=p)
  })



penalizacion_t=function(id_estacion, h=1, p=1, t=1){
  source("lib/calcula_penalizacion.R")
  library(dplyr)
  library(skellam)
  print(id_estacion)
  capacidad=unique(dat%>% filter(estacion==id_estacion) %>% select(capacidad)) %>%as.numeric()
  res_temp=sapply(0:capacidad,calcula_penalizacion_acumulada,dat=dat,id_estacion=id_estacion, hora_max=t+1, hora_min=t,h=h, p=p) 
  res_temp=rbind(0:capacidad , res_temp) %>% t() %>% data.frame() %>%
    mutate(estacion=id_estacion) %>% setNames(c("i0","penalizacion","estacion"))
  return(res_temp)
}


parametros=expand.grid(t=5:24,estacion=seleccionadas) 
linearizacion=parametros %>%
  pmap_dfr(function(t,estacion){
    tryCatch({
      res <- penalizacion_t(id_estacion = estacion, t=t)
      res %>%
        mutate(penalizacion1=lead(penalizacion,1),
               b_i=penalizacion1-penalizacion, 
               a_i=penalizacion- b_i*i0,t=t) %>%
        select(i0,estacion,b_i,a_i,penalizacion,t=t)
    },error=function(e){})
  })
saveRDS(linearizacion,file = paste0("cache/penalizacion/linearizacion.RDS"))
# saveRDS(res,file = paste0("cache/penalizacion/penalizacion",t,".RDS"))




estaciones_datos=data.table::fread("data/estaciones.csv")

capacidades=dat %>% select(estacion,capacidad) %>%
  unique()
dat=res %>%
  left_join(estaciones_datos %>%select( id, name, address, `location/lat`,`location/lon`), by=c("estacion"="id")) %>%
  left_join(capacidades, by=c("estacion")) %>%
  mutate(llenado=i0/capacidad) %>%
  group_by(estacion) %>%
  filter(penalizacion==min(penalizacion))


plot(y=temp$penalizacion,x=temp$i0)


save(dat,file = "data/base_penalizacion.RData")





n=length(dat$estacion)
f.obj <- rep(1,n) %>%
  c(rep(0,n))
f.con <- linearizacion %>%
  # filter(!is.na(a_i) & !is.na(b_i)) %>%
  mutate(id=paste0(estacion,i0,
                   a_i=ifelse(is.na(a_i),0,a_i) ,
                   b_i=ifelse(is.na(b_i),0,b_i))) %>%
  select(b_i,estacion,id,a_i) %>% 
  group_by(id,a_i) %>%
  tidyr::spread(estacion,b_i) 
f.con[is.na(f.con)]=0 
f.rhs=f.con$a_i

f.con =f.con %>%
  ungroup() %>%
  select(-c(id,a_i)) %>%
  as.matrix()

f.con[is.na(f.con)]=0 

f.dir <- rep(">=", nrow(f.con)) 
#coeficientes de s_i
temp=f.con
temp[temp!=0]=-1
f.con=f.con %>%
  cbind(temp)


##para min y max
temp=diag(x=1, nrow = n,ncol=2*n)

f.con=f.con %>%
  rbind(temp) %>%
  rbind(temp)
f.con[is.na(f.con)]=0
f.con[is.infinite(f.con)]=0
f.con[is.nan(f.con)]=0

f.rhs= f.rhs %>%
  c(capacidades$capacidad) %>%
  c(rep(0, n))


f.dir <- f.dir %>%
  c(rep("<=", n)) %>%
  c(rep(">=", n))
library(lpSolve)
sol=lp ("min", f.obj, f.con, f.dir, f.rhs)



