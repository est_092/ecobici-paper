##Función objetivo

f <- function(t=t_mat, alpha=0,n) {
  # f <- function(x) {
  # sum(x[(2*n^2 +(2*n)+1) :(2*n^2 +3*n)  ])  + ( alpha  *  sum(as.vector(t) )
  return (c( 
    ##T _ij
    alpha*as.vector(t(t)),
    rep(0, n^2),
    rep(0, n),
    rep(0, n),
    rep(0, n),
    rep(1, n),
    rep(0, n)))
}

##Primera restricción
##Inventory Balance    n
## = si0
r2=function(n){
  res=NULL
  for (n1 in 0:(n-1)){
    temp=rep(0,2*n^2+5*n)
    temp[2*n^2 +2*n +1 +n1]=1 ##si
    temp[2*n^2 +1 + n1]=+1    ##y_load
    temp[2*n^2 + n+1 +n1]=-1   ##y_unload
    res=res %>%
      rbind(temp)
  }
  return (res)
}

##Conservation of inventory in each vehicle  n    
## = 0
r3=function(n){
  res=NULL
  for (n1 in 0:(n-1)){
    temp=rep(0,2*n^2+5*n)
    temp[(n^2 +1 + n*n1): (n^2 +n + n*n1)]=-1 ##yij   Suma sobre j excepto i=j
    temp[(n^2 +1 + n*n1) + (n1) ]= 0
    
    
    temp[ seq(from=(n^2 +1 + n1),  to=(2* n^2 - (n-1)  +n1 ), by= n)]=1 ##yji   Suma sobre j excepto i=j
    temp[(n^2 +n1 +1 + n*n1)]= 0
    
    temp[2*n^2 + n1 +1 ]=1    ##y_load
    temp[2*n^2 + n+1 +n1]=-1   ##y_unload
    res=res %>%
      rbind(temp)
  }
  return (res)
}
##Vehicle capacity
##Se usa setdiff para i!=j    n*n - n
## <= 0
# x[setdiff((n^2 +1) :(2*n^2) , seq(from=(n^2 +1) , to=(2*n^2), by= (n+1) ))] - 
#   k * x[ setdiff(1:n^2 , seq(from=(1) , to=(n^2), by= (n+1) ))]

r4=function(n,k){
  res=NULL
  for (n1 in 0: (n-1) ){
    for(n2 in 0: (n-1)){
      if(n1 != n2){
        temp=rep(0,2*n^2+5*n)
        temp[ n*n1  +1+ n2  ]=-k
        temp[ n^2 + n*n1  +1+ n2  ]=1
        res=res %>%
          rbind(temp)
      }
      
    }
  }
  # for (n1 in 0:(n^(2)-1)){
  #   
  #   
  #   temp=rep(0,2*n^2+5*n)
  #   if(!n1 %in% seq(from=(n^2 +1), to=(2*n^2), by= (n+1) )){
  #   temp[ n1 +1  ]=-k  ##Para cada xij  excepto i=j
  #   temp[(n^2 +n1 +1)]= 1 ##Para cada yij    excepto i=j
  #   res=res %>%
  #     rbind(temp)
  #   }
  # }
  return (res)
}
##Vehicle flow conservation  n
## = 0

r5=function(n){
  res=NULL
  for (n1 in 0: (n-1) ){
    temp=rep(0,2*n^2+5*n)
    temp[ (n*n1 +1) : (n *n1 + n )]=1  ##Suma sobre i de xij  excepto i=j
    temp[seq(n1 +1 ,n^2 , by=n)]=-1 ##Suma sobre j de xji  excepto i=j
    temp[n1*n + n1 +1]=0
    res=res %>%
      rbind(temp)
  }
  return (res)
}

##Each station is visited once at most by vehicle n
## <= 1
r6=function(n){
  res=NULL
  # for (n1 in 0: (n-1) ){
  for (n1 in 1: (n-1) ){
    temp=rep(0,2*n^2+5*n)
    temp[ (n*n1 +1) : (n *n1 + n )]=1  ##Suma sobre i de xij  excepto i=j
    temp[n1*n + n1 +1]=0
    res=res %>%
      rbind(temp)
  }
  return (res)
}


## Quantity picked up at station n 
## <= si0
r7=function(n){
  res=NULL
  for (n1 in 0: (n-1) ){
    temp=rep(0,2*n^2+5*n)
    temp[ (2*n^2)+1+n1]=1  ##Suma sobre v de yi_U para cada i
    temp[n1*n + n1 +1]=0
    res=res %>%
      rbind(temp)
  }
  return (res)
}

## Quantity  unloaded at station    n
## <= ci - si0
r8=function(n){
  res=NULL
  for (n1 in 0: (n-1) ){
    temp=rep(0,2*n^2+5*n)
    temp[ ((2*n^2)+1+n1+n)]=1  ##Suma sobre v de yi_L para cada i
    temp[n1*n + n1 +1]=0
    res=res %>%
      rbind(temp)
  }
  return (res)
}


## All bicycles loaded are unloaded   1
## = 0 
r9=function(n){
  res=NULL
  # for (n1 in 0: (n-1) ){
  temp=rep(0,2*n^2+5*n)
  temp[( (2*n^2)+1):  ((2*n^2)+n)]=1  ##Suma sobre v de yi_U para cada i
  temp[ (((2*n^2)+1+n)): (((2*n^2)+2*n))]=-1  ##Suma sobre v de yi_L para cada i
  res=res %>%
    rbind(temp)
  # }
  return (res)
}


## Time Constraint  1
## <= T 
r10=function(n,L=15,U=15, t){
  res=NULL
  temp=rep(0,2*n^2+5*n)
  temp[ ((2*n^2)+1):((2*n^2)+n)]=L  ##Suma sobre v de yi_U para cada i
  temp[ ((2*n^2)+1+n): ((2*n^2)+2*n)]=U  ##Suma sobre v de yi_L para cada i
  temp[1:n^2]=as.vector(t(t))
  temp[seq(1,n^2, by=(n+1))]=0
  return (temp)
}


##Sub - Tour Elimination n*n - (2n -1 )
## >= -1 +M 
r11=function(n,M=n){
  res=NULL
  for (n1 in 0: (n-1) ){
    for(n2 in 1: (n-1)){
      if(n1 != n2){
        temp=rep(0,2*n^2+5*n)
        temp[2*n^2 + 4*n +1 +n1]=-1
        temp[2*n^2 + 4*n +1 +n2]=+1
        temp[n1*n +n2 +1 ]=-M
        res=res %>%
          rbind(temp)
      }
      
    }
  }
  return (res)
}

r12=function(n,linearizacion=linear){
  res=NULL
  # i=0
  for (n1 in 2: n ){
    ci=linearizacion %>%
      filter(id==n1)%>%
      summarise(n=max(i0)) %>%.$n
    
    for(u in 0: (ci -1) ){
      temp=rep(0,2*n^2+5*n)
      temp[2*n^2 +2*n  +n1]= linearizacion %>%                ##s_i
        filter(id==n1 & i0==u)%>% .$b_i  *(-1)
      temp[2*n^2 +3*n +n1]=1   ##g_i
      res=res %>%
        rbind(temp)
      # i=i+1
    }
    
    
  }
  return (res)
}
rhs_r12=function(n,linearizacion=linear){
  res=NULL
  # i=0
  for (n1 in 2: n ){
    ci=linearizacion %>%
      filter(id==n1)%>%
      summarise(n=max(i0)) %>%.$n
    
    for(u in 0: (ci -1) ){
      temp= linearizacion %>%                ##s_i
        filter(id==n1 & i0==u)%>% .$a_i  
      res=c(res,temp)
      # i=i+1
    }
    
    
  }
  return (res)
}