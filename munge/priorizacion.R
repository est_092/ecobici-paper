load("data/base.RData")


priorizacion=dat %>%
  mutate(tiempo=ifelse(horas_retiro>=14,"tarde","mañana")) %>%
  select(Ciclo_Estacion_Retiro, Ciclo_Estacion_Arribo, tiempo) %>% 
  gather(key, value , -c(tiempo)) %>%
  select(value, tiempo) %>%
  group_by(value,tiempo) %>%
  tally() %>%
  spread(tiempo,n) %>%
  mutate(total=mañana+tarde) %>%
  ungroup() %>%
  mutate_at(vars(total,mañana,tarde),funs(rank=dense_rank(desc(.)))) %>%
  arrange(desc(total_rank)) %>%
  rename(estacion=value)

save(priorizacion,file="cache/priorizacion.RData")

# %>%
#   mutate(gap=(mañana - tarde),
#          clasificacion=ifelse(gap>1000, "mañana",ifelse(gap<-1000,"tarde","neutro"))) 

