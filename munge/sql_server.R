library(chron)
library(dplyr)
library(data.table)
library(RODBC)

dat_full=NULL
files= list.files("./data/bases")
for (file in files){
  dat=fread(paste0("./data/bases/",file)) %>%
     select(Ciclo_Estacion_Retiro, Fecha_Retiro ,Hora_Retiro,Ciclo_Estacion_Arribo,Fecha_Arribo,Hora_Arribo)
  ##Hacemos que los tiempos sean formato POSIXlt
  if(nchar(strsplit(as.character(dat$Fecha_Retiro[1]), "/")[[1]][1])==2){
    dat$Fecha_Retiro=as.Date(dat$Fecha_Retiro,"%d/%m/%Y")
    dat$Fecha_Arribo=as.Date(dat$Fecha_Arribo,"%d/%m/%Y")
  }
  # dat$Fecha_Retiro=as.factor(format(dat$Fecha_Retiro,"%Y-%m-%d"))
  dat$Hora_Retiro=paste0(dat$Fecha_Retiro," ",dat$Hora_Retiro)
  dat$Hora_Retiro=strptime(dat$Hora_Retiro, format="%Y-%m-%d %H:%M:%S")
  dat$Fecha_Retiro=NULL
  dat$Hora_Arribo=paste0(dat$Fecha_Arribo," ",dat$Hora_Arribo)
  dat$Hora_Arribo=strptime(dat$Hora_Arribo, format="%Y-%m-%d %H:%M:%S")
  dat$Fecha_Arribo=NULL
  dat_full=plyr::rbind.fill(dat_full,dat)
  # con=odbcConnect("sql69", uid = "sa", pwd = "axelrose69")
    # dbWriteTable(con, "viajes", dat, overwrite = FALSE,append=TRUE,row.names=FALSE) 
  # dbDisconnect(con)
}

save(dat_full, file="dat/base.RData")
