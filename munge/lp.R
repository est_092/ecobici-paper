library(dplyr)
library(scam)

load("data/base_penalizacion.RData")
dat_temp=dat %>% 
  filter(estacion==271)


MonoConvexSpline <- scam(penalizacion~s(i0,k=4,bs="mdcx",m=1),data=dat_temp)



MonoConvexSplinePredict =function(Test){
  predict.scam(MonoConvexSpline,data.frame(x = Test))
} 
#Getting Ready for plotting Monotonic Convex Spline
MonoConvexSplineResult = MonoConvexSplinePredict(head(dat_temp))



MonoConvexSplineResult = predict.scam(MonoConvexSpline,dat_temp)


Plot = qplot(xlab = "x", ylab = "y")
Plot = Plot + geom_line(aes(xArray,MonoResult            , colour = "Monotonic Spline"            ))
Plot = Plot + geom_line(aes(xArray,MonoConvexSplineResult, colour = "Monotonic Convex scam Spline"))
Plot = Plot + geom_line(aes(xArray,spCobsResults         , colour = "Monotonic Convex cobs Spline"))
Plot
