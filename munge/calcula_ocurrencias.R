##Obtener las matrices por periodos.
horas=c(0:23)
minutos=c(00,15,30,45,60)
for ( hora in horas)
{
  # hora=16
  
  ##Te quedas solo con la matriz de la hora que necesitas
  temp_horas=which(hours(DatosPol$Hora_Arribo)==hora )
  DatosTemp_horas=DatosPol[temp_horas,]
  for(periodo in 1:4)
  {
    ##periodo=2
    
    ##En cada per?odo nos quedaremos solo con la matriz de los periodos correspondientes.
    temp_min=which(minutes(DatosTemp_horas$Hora_Arribo)>=minutos[periodo] & minutes(DatosTemp_horas$Hora_Arribo)<=minutos[periodo+1] )
    DatosTemp_minutos=DatosTemp_horas[temp_min,]
    ##Contamos ocurrencias para construir la matriz de transici?n.
    ocurrencias=matrix(0,nrow = length(Polanco), ncol = length(Polanco))
    columna=0
    renglon=0
    for (salida in Polanco)
    { 
      renglon=renglon+1
      for (llegada in Polanco) 
      {   
        columna=columna+1
        ocurrencias[renglon,columna]=sum(DatosTemp_minutos$Ciclo_Estacion_Retiro == salida & DatosTemp_minutos$Ciclo_Estacion_Arribo==llegada)
      }
      columna=0
    }
    for( i in 1:length(ocurrencias[,1]))
    {
      if(sum(ocurrencias[i,])>0){
        ocurrencias[i,]=ocurrencias[i,]/sum(ocurrencias[i,])
      }
    }
    nombre=paste0("./data/ocurrencias/",hora,"_",periodo,".csv")
    write.csv(ocurrencias,file = nombre)
  }
}
