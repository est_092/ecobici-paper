library(gmapsdistance)
# Polanco=c(196,198,199,200,201,202,203,205,206,207,208,209,210,211,212,213,214,215,216,217,218
#           ,219,220,221,222,223,224,225,226,228,229,230,231,233,235,236,239,240,241,451)
estaciones=read.csv("data/estaciones.csv") 
Polanco=estaciones %>% filter(districtName=="Polanco") %>% .$id
set.api.key("AIzaSyB6BLEl5Bhygcrfy-Q4USZ6dMghajMNax0")


estaciones=estaciones %>%
  filter(id %in% Polanco) %>%
  mutate(coordenadas=paste0(`location.lat`,",",`location.lon`), 
         longitud=location.lon,
         latitud=location.lat) 


# temp=estaciones%>%
#   filter(id<=n) %>%
#   mutate(coordenadas=paste0(`location.lat`,",",`location.lon`))


results = gmapsdistance(estaciones$coordenadas, estaciones$coordenadas, mode = "driving", shape = "long")
results

saveRDS(results, file="cache/maps_Polanco.RDS")



# save(results, file="cache/maps_primeras_20.RData")

load("cache/maps_primeras_20.RData")

temp=estaciones%>%
  filter(id<=20) %>%
  mutate(coordenadas=paste0(`location.lat`,",",`location.lon`))

matrix_dist=results$Time %>%
  left_join(temp%>% select(id, coordenadas), by=c("or"="coordenadas")) %>%
  left_join(temp%>% select(id, coordenadas),  by=c("de"="coordenadas"))
test=matrix_dist %>%
  select(x=id.x,y=id.y, time=Time) %>%
  filter(x<=n & y<=n) %>%
  arrange(x,y) %>%
  spread(y,time) %>%
  select(-x) %>%
  as.matrix()

rownames(test)=test$x

test=test%>%
  select(-x) %>%
  as.matrix()

capacity=read.csv("data/disponibilidad.csv")

capacity=capacity %>%
  mutate(capacity=availability.bikes+availability.slots) %>%
  select(id, capacity)
save(capacity,file="cache/capacity.RData")



s0=read.delim("~/ecobici/data/estado_01am.csv") %>% 
  select(estacion=id, s0=availability__bikes)
save(s0,file="cache/s0.RData")
